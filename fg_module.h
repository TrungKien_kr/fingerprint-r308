/*
 * Author: KienLTb
 * Date: 2017-12-29 21:45:42
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <stdexcept>

#include "lib/SYProtocol.h"

using namespace std;

#define DEV_ADDR 0xffffffff

class fg_module
{
  public:
    fg_module(int dev_type_, string com_port_, int baudrate_ = 57600)
        : dev_type(dev_type_), com_port(com_port_),
          baudrate(baudrate_ / 9600) /* SYProtocol.a take 0-9600, 1-19200 ... */
    {
        if (PSOpenDevice(dev_type, com_port.c_str(), baudrate) == FALSE)
            throw runtime_error("Cannot open device");
        else
            cout << "Open " << com_port_ << " success!" << endl;
    }

    ~fg_module()
    {
        PSCloseDevice();
    }

    /* Validate password of module*/
    int validate_pwd(unsigned char* pwd);

    /* Get fingerprint and store to internal buffer */
    int get_fingerprint();

    /* Enroll fingerprint, get 2 samples of fingerprint combine and genChar then store to both bufferA and bufferB*/
    int enroll_fingerprint();

    /* Capture fingerprint and store to bmp file */
    int capture_fingerprint_image(string bmp_path);

    /* Load image from bmp_path and genchar to bufferID */
    int gen_char_from_bmp(string bmp_path, int iBfferID);

    /* Validate fingerprint from bmp file */
    int validate_from_image(string bmp_path);

    /* Capture fingerprint char and store to char file */
    int capture_fingerprint_char(int argc, char* const argv[]);

    /* Load fingerprint character from char file */
    int load_char_from_file(string char_path, int iBfferID);

    /* Validate fingerprint from char file */
    int validate_from_char(int argc, char* const argv[]);

  private:
    int dev_type;
    string com_port;
    int baudrate;
};
