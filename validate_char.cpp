/*
 * Author: KienLTb
 * Date: 2018-01-13 8:45:42
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <stdexcept>
#include <unistd.h>

#include <getopt.h>

#include "fg_module.h"

#define CMD_CREATE   0
#define CMD_VALIDATE 1

using namespace std;

const char* s_opt = "hc:v:";
const struct option l_opt[] = {
        {"help",     0, NULL, 'h'},
        {"capture",  1, NULL, 'c'},
        {"validate", 1, NULL, 'v'},
};

static int usage(const char* name)
{
    cout << "Fingerprint R307 module - Demo application\n" << endl;
    cout << "Usage:\n\t";
    cout << name << " -c [--capture]  finger.char    Capture character of fingerpeint to image.bmp\n\t";
    cout << name << " -v [--validate] finger.char    Validate finger from finger.char\n\t";
    cout << name << " -h [--help]                    Print help and exit\n";

    return 0;
}

int main_app(int argc, char* const argv[])
{

    if (argc < 2)
        return usage(argv[0]);

    /* Resetime 200ms - page3 Datasheet*/
    usleep(200000);

    int cmd = -1;
    string cmd_arg;
    string serial_port = "/dev/ttyUSB0"; /* Default serial port */
    unsigned char default_pwd[5] = {0};

    fg_module demo(DEVICE_COM, serial_port, 57600);
    demo.validate_pwd(default_pwd);

    int opt;
    while ((opt = getopt_long(argc, argv, s_opt, l_opt, NULL)) != -1) {
        switch (opt) {
        case 'c': /* Capture fingerprint image */
            return demo.capture_fingerprint_char(argc, argv);
        case 'v': /* Validate fingerprint from image */
            /* NOTE: Hardcore first option is command */
            return demo.validate_from_char(argc, argv);
        case 'h':
        case '?':
            usage(argv[0]);
            exit(0);
        case -1:
            break;
        default:
            abort();
        }
    }

    cout << "Not support command: " << cmd << endl;
    exit(-1);
}


int main(int argc, char* const argv[])
{
    try {
        main_app(argc, argv);
    } catch (runtime_error& e) {
        cout << e.what() << '\n';
    } catch (...) {
        cout << "exiting\n";
    }

    return EXIT_SUCCESS;
}
