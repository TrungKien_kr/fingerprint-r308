
all: validate_image validate_char

validate_image: validate_image.cpp fg_module.o
	g++ -g -c validate_image.cpp -o validate_image.o
	g++ -g  validate_image.o fg_module.o lib/SYProtocol.a -o validate_image

validate_char: validate_char.cpp fg_module.o
	g++ -g -c validate_char.cpp -o validate_char.o
	g++ -g  validate_char.o fg_module.o lib/SYProtocol.a -o validate_char


fg_module.o: fg_module.cpp fg_module.h
	g++ -g -c fg_module.cpp -o fg_module.o
clean:
	rm -rf *.o *.bmp  *.dat validate_image validate_char
