/*
 * Author: KienLTb
 * Date: 2017-12-29 21:45:42
 */

#include <cstdio>
#include "fg_module.h"

/* Capture fingerprint and store to bmp file */
int fg_module::capture_fingerprint_image(string bmp_path)
{
    cout << "Get fingerprint and store to " << bmp_path << endl;

    this->get_fingerprint();

    cout << "\tTranfering image and store to " << bmp_path << "..." << endl;
    unsigned char pImage[256][288];
    int nImageLen;
    int ret;

    ret = PSUpImage(DEV_ADDR, pImage[0], &nImageLen);
    if (ret != PS_OK)
        throw runtime_error("PSUpImage Failed: " + string(PSErr2Str(ret)));

    ret = PSImgData2BMP(pImage[0], bmp_path.c_str());
    if (ret != PS_OK)
        throw runtime_error("PSImgData2BMP Failed: " + string(PSErr2Str(ret)));

    cout << "done!" << endl;
    usleep(3000);

    return 0;
}


/* Validate fingerprint from bmp file */
int fg_module::validate_from_image(string bmp_path)
{
    unsigned char pImage[256][288];
    int nImageLen;
    int ret;

    cout << "Verify fingerprint from image: " << bmp_path << endl;

    /* Get 2 sample of finger, combine and genChar then store to both bufferA and bufferB */
    this->enroll_fingerprint();

    /* Load image from bmp_path and genchar to bufferA */
    this->gen_char_from_bmp(bmp_path, CHAR_BUFFER_B);

    /* Compare finger character in bufferA and bufferB */
    int score;
    ret = PSMatch(DEV_ADDR, &score);
    cout << "\nResult: " <<  (ret == PS_OK ? "Match " : "Not match ");
    cout  << "Score: " << score << endl;

    return 0;
}


/* Capture fingerprint, genchar and store to char file */
int fg_module::capture_fingerprint_char(int argc, char* const argv[])
{
    /* Hardcore first arg is command -c or -v */
    for (int i = 2; i < argc; ++i) {
        string char_path = argv[i];
        cout << "Get fingerprint character and store to " << char_path << endl;

        /* Get 2 sample of finger, combine and genChar then store to both
         * bufferA and bufferB */
        this->enroll_fingerprint();

        unsigned char pchar_data[512] = {0};
        int char_size;
        int ret;

        ret = PSUpChar(DEV_ADDR, CHAR_BUFFER_A, pchar_data, &char_size);
        if (ret != PS_OK)
            throw runtime_error("PSUpChar Failed: " + string(PSErr2Str(ret)));

        /* Write fingerprint character to file */
        FILE* fp = fopen(char_path.c_str(), "wb");
        if (!fp)
            throw runtime_error("Failed to open " + char_path);

        fwrite(pchar_data, 1, char_size, fp);

        fclose(fp);

        cout << "--------------------------------------------------------"
             << endl;
        cout << "Success to save fingerprint character to " + char_path << endl;
        cout << "--------------------------------------------------------"
             << endl;
    }

    return 0;
}


/* Validate fingerprint from char file */
int fg_module::validate_from_char(int argc, char* const argv[])
{
    cout << "Verify fingerprint from character" << endl;

    this->get_fingerprint();

    int ret = PSGenChar(DEV_ADDR, CHAR_BUFFER_A);
    if (ret != PS_OK) {
        string err = "PSGenChar Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
    usleep(1000);

    cout << "--------------------------------------------------------" << endl;
    /* Hardcore first arg is command -c or -v */
    for(int i = 2; i < argc; ++i) {
        string char_path = argv[i];
        /* Load image from char_path and genchar to buffer ID */
        this->load_char_from_file(char_path, CHAR_BUFFER_B);

        /* Compare finger character in bufferA and bufferB */
        int score;
        ret = PSMatch(DEV_ADDR, &score);

        cout << "file: "<< char_path << " Result: " <<  (ret == PS_OK ? "Match " : "Not match ");
        cout << "Score: " << score << endl;
    }
    cout << "--------------------------------------------------------" << endl;

    return 0;
}


int fg_module::load_char_from_file(string char_path, int buffer_ID)
{
    unsigned char pchar_data[512]={0};
    int char_size;
    int ret;

    FILE* fp = fopen(char_path.c_str(), "rb");
    if (!fp)
        throw runtime_error("Failed to open " + char_path);

    memset(pchar_data, 0, 512);
    fread(pchar_data, 1, 512, fp);
    fclose(fp);

    ret = PSDownChar(DEV_ADDR, buffer_ID, pchar_data, 512);
    if (ret != PS_OK){
        string err = "PSDownChar Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
}


/* Validate password of module*/
int fg_module::validate_pwd(unsigned char* pwd)
{
    int ret = PSVfyPwd(DEV_ADDR, pwd);
    if (ret != PS_OK) {
        string err = "Failed to connect device or invalid password: "
                     + string(PSErr2Str(ret));
        throw runtime_error(err);
    }

    usleep(10000);
    return 0;
}


/* Get fingerprint and store to internal buffer */
int fg_module::get_fingerprint()
{
    cout << "Please press finger" << endl;
    while ((PSGetImage(DEV_ADDR)) == PS_NO_FINGER) {
        usleep(10);
    }
    cout << "\tSuccess to get fingerprint" << endl;

    return 0;
}


/* Enroll fingerprint, get 2 samples of finger to genChar and store to flash */
int fg_module::enroll_fingerprint() {
    int ret;

    cout << "Get sample fingerprint 1: ";
    this->get_fingerprint();

    ret = PSGenChar(DEV_ADDR, CHAR_BUFFER_A);
    if (ret != PS_OK) {
        string err = "PSGenChar Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
    usleep(1000);

    cout << "Get sample fingerprint 2: ";
    this->get_fingerprint();

    ret = PSGenChar(DEV_ADDR, CHAR_BUFFER_B);
    if (ret != PS_OK) {
        string err = "PSGenChar Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
    usleep(1000);

    cout << "Generating the templet from bufferA and bufferB " << endl;
    ret = PSRegModule(DEV_ADDR);
    if(ret != PS_OK) {
        string err = "PSRegModule Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
    cout << "\tSuccess to Generate templet" << endl;

    usleep(1000);

    return 0;
}


/* Load image from bmp_path and genchar to bufferID */
int fg_module::gen_char_from_bmp(string bmp_path, int iBfferID) {
    unsigned char pImage[256][288];
    int nImageLen;
    int ret;

    cout << "Loading image from " << bmp_path << " to buffer..." << endl;

    ret = PSGetImgDataFromBMP(bmp_path.c_str(), pImage[0], &nImageLen);
    if (ret != PS_OK) {
        string err = "PSGetImgDataFromBMP Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }

    cout << "\tDownloading image to module..." << endl;

    ret = PSDownImage(DEV_ADDR, pImage[0], nImageLen);
    if (ret != PS_OK) {
        string err = "PSDownImage Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }
    usleep(1000);

    cout << "Validating image ..." << endl;

    ret = PSGenChar(DEV_ADDR, iBfferID);
    if (ret != PS_OK) {
        string err = "PSGenChar Failed: " + string(PSErr2Str(ret));
        throw runtime_error(err);
    }

    return 0;
}
