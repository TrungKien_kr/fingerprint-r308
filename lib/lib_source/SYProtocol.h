#ifndef  _SYPROTOCOL_H_
#define  _SYPROTOCOL_H_


///////////////////return code////////////////////
#define PS_OK                0x00
#define PS_COMM_ERR          0x01
#define PS_NO_FINGER         0x02
#define PS_GET_IMG_ERR       0x03
#define PS_FP_TOO_DRY        0x04
#define PS_FP_TOO_WET        0x05
#define PS_FP_DISORDER       0x06
#define PS_LITTLE_FEATURE    0x07
#define PS_NOT_MATCH         0x08
#define PS_NOT_SEARCHED      0x09
#define PS_MERGE_ERR         0x0a
#define PS_ADDRESS_OVER      0x0b
#define PS_READ_ERR          0x0c
#define PS_UP_TEMP_ERR       0x0d
#define PS_RECV_ERR          0x0e
#define PS_UP_IMG_ERR        0x0f
#define PS_DEL_TEMP_ERR      0x10
#define PS_CLEAR_TEMP_ERR    0x11
#define PS_SLEEP_ERR         0x12
#define PS_INVALID_PASSWORD  0x13
#define PS_RESET_ERR         0x14
#define PS_INVALID_IMAGE     0x15
#define PS_HANGOVER_UNREMOVE 0X17


///////////////buffer zone//////////////////////////////
#define CHAR_BUFFER_A          0x01
#define CHAR_BUFFER_B          0x02
#define MODEL_BUFFER           0x03

//////////////////serial com////////////////////////
#define COM1                   0x01
#define COM2                   0x02
#define COM3                   0x03

//////////////////baud rate setting////////////////////////
#define BAUD_RATE_9600         0x00
#define BAUD_RATE_19200        0x01
#define BAUD_RATE_38400        0x02
#define BAUD_RATE_57600        0x03   //default
#define BAUD_RATE_115200       0x04

#define WINAPI

typedef unsigned char BYTE;

#define IMAGE_X 256
#define IMAGE_Y 288

#define TRUE  1
#define FALSE 0

#define DEVICE_USB   0
#define DEVICE_COM   1
#define DEVICE_UDisk 2

/////////////////////////////////////////
//for open:0-false 1- true
int WINAPI PSOpenDevice(int nDeviceType,const char *pPort,int nPortPara,int nPackageSize=2);
int WINAPI PSCloseDevice();
void WINAPI Delay(int nTimes);
///////////////////////////////////////////////
//////           function                //////
///////////////////////////////////////////////
/* Detect finger and Get Image */
int WINAPI PSGetImage(int nAddr);

/* Generate Character file */
int WINAPI PSGenChar(int nAddr, int iBufferID);

/* Match two character file on chip */
int WINAPI PSMatch(int nAddr, int* iScore);

/* Search a part or all of fingerprint libray */
int WINAPI PSSearch(int nAddr, int iBufferID, int iStartPage, int iPageNum, int* iMbAddress);

/* Combine BufferA’s character file with BufferB’s character file and generate the templet */
int WINAPI PSRegModule(int nAddr);

/* Store BufferA or BufferB’s character file to flash fingerprint library */
int WINAPI PSStoreChar(int nAddr, int iBufferID, int iPageID);

/* Transfer a templet to BufferA or BufferB from flash fingerprint library */
int WINAPI PSLoadChar(int nAddr, int iBufferID, int iPageID);

/* Transfer character file from BufferA or BufferB to PC */
int WINAPI PSUpChar(int nAddr, int iBufferID, unsigned char* pTemplet, int* iTempletLength);

/* Download a character file form pc to BufferA or BufferB */
int WINAPI PSDownChar(int nAddr, int iBufferID, unsigned char* pTemplet, int iTempletLength);

/* Transfer character file from BufferA or BufferB to PC and write to file */
int WINAPI PSUpChar2File(int nAddr, int iBufferID, const char* pFileName);

/* Download a character file form pc to BufferA or BufferB */
int WINAPI PSDownCharFromFile(int nAddr, int iBufferID, const char* pFileName);

/* Delete specify range of character file from flash fingerprint libaray */
int WINAPI PSDelChar(int nAddr, int iStartPageID, int nDelPageNum);

/* Clear flash fingerprint libaray */
int WINAPI PSEmpty(int nAddr);

/* Transfer image file from BufferA or BufferB to PC */
int WINAPI PSUpImage(int nAddr, unsigned char* pImageData, int* iImageLength);

/* Download a image file form pc to BufferA or BufferB */
int WINAPI PSDownImage(int nAddr, unsigned char* pImageData, int iLength);

/* Convert Imagebuffer to bmp file */
int WINAPI PSImgData2BMP(unsigned char* pImgData, const char* pImageFile);

/* Read bmp file to Image buffer */
int WINAPI PSGetImgDataFromBMP(const char* pImageFile, unsigned char* pImageData, int* pnImageLen);

/* Set chip password */
int WINAPI PSSetPwd(int nAddr, unsigned char* pPassword);

/* Verify Device Communicate Key */
int WINAPI PSVfyPwd(int nAddr, unsigned char* pPassword);

/* Set chip address */
int WINAPI PSSetChipAddr(int nAddr, unsigned char* pChipAddr);

/* Reset chip */
int WINAPI PSReset(int nAddr);

/* Power down chip */
int WINAPI PSPowerDown(int nAddr);

/* Read Notepad */
int WINAPI PSReadInfo(int nAddr, int nPage, unsigned char* UserContent);

/* Write Notepad */
int WINAPI PSWriteInfo(int nAddr, int nPage, unsigned char* UserContent);

int WINAPI PSReadParTable(int nAddr, unsigned char* pParTable);

/* Set uart baudrate */
int WINAPI PSSetBaud(int nAddr, int nBaudNum);

/* Set security level */
int WINAPI PSSetSecurLevel(int nAddr, int nLevel);

/* Set size of package */
int WINAPI PSSetPacketSize(int nAddr, int nSize);

/* Get random data generate by chip */
int WINAPI PSGetRandomData(int nAddr, unsigned char* pRandom);

/* Format error information */
char* WINAPI PSErr2Str(int nErrCode);

#endif
