## Fingerprint - S308
Demo project fingerprint S308

1. Capture fingerprint image và lưu thành file bmp.
2. Lấy 2 mẫu fingerprint để tạo đặc tính sau đó lưu vào bufferA. Tạo đặc tính từ file ảnh và lưu vào bufferB. Dùng hàm Match so sánh 2 đặc tính lưu ở buffer để xác thực.

### Chuẩn bị

- Kết nối RPI vs S308 fingerprint

  ![Connection](doc/image/connection.png)

- Disable RPI uart console [link](https://www.cube-controls.com/2015/11/02/disable-serial-port-terminal-output-on-raspbian/)
- Login vào RPi, thực hiện clone và build source code

        git clone https://TrungKien_kr@bitbucket.org/TrungKien_kr/fingerprint-r308.git
        cd fingerprint-r308
        make

 - App demo:

        Usage:

        ./demo -c [--capture]  image.bmp    Capture fingerprint image and store to image.bmp
        ./demo -v [--validate] image.bmp    Validate finger from image.bmp
        ./demo -p [--port]     /dev/tty*    Set serial port
        ./demo -h [--help]                  Print help and exit

- Capture finger và lưu vào file bmp

        $ ./demo --capture ngon_tro.bmp
        or
        $ ./demo -c ngon_tro.bmp

- Validate finger từ file bmp

        $ ./demo --validate ngon_tro.bmp
        or
        $ ./demo -v ngon_tro.bmp

- Trong trường hợp muốn sử dụng serial port khác, sử dụng option --port

        $ ./demo --port /dev/ttyUSB0 --validate ngon_tro.bmp
        or
        $ ./demo --port /dev/ttyUSB0 -v ngon_tro.bmp

- sample result
  - Pass

       ![Pass](doc/image/result.png)

  - Fail

       ![Fail](doc/image/result_fail.png)

### Note
- Sửa API `PSOpenDevice`: Thay vì truyền portnumber thì truyền đường dẫn của port (Lý do, đường dẫn linh hoạt hơn portnumer, không cần thêm define một số tương ứng với port mới)

        /* Org API */
        int WINAPI PSOpenDevice(int nDeviceType,int nPortNum,int nPortPara,int nPackageSize=2);
        /* New API */
        int WINAPI PSOpenDevice(int nDeviceType,const char *pPort,int nPortPara,int nPackageSize=2);

- Source code và file demo gốc lưu ở `/lib/lib_source/`
### Reference
Các tài liệu tham khảo lưu ở thư mục doc

- `doc/API for Linux(UNIX) user manual`
- `doc/ZFM-user-manualV15`
- `doc/R307-fingerprint-module-user-manual`
